const mongoose = require("mongoose");
const dotenv = require("dotenv");
dotenv.config({ path: "./config.env" });
const app = require("./app");
// const { error } = require("console");

const DB = process.env.DATABASE.replace(
  "PASSWORD",
  process.env.DATABASE_PASSWORD
);
//Mongodb atlas
// console.log(process.env.DATABASE_PASSWORD)
mongoose
  .connect(DB)
  .then((con) => {
    // console.log(con.connections)
    console.log("DB connection successful");
  })
  .catch((error) => console.log(error));

// Local MongoDB
// const local_DB = process.env.DATABASE_LOCAL
// //console.log(proces.env.DATABASE_PASSWORD)
// mongoose.connect(local_DB).then((con) => {
//     // console.log(con.connections)
//     console.log("DB connection successful")
// }).catch(error => console.log(error))

/* Starting the server on port 4002. */
const port = 4001;
app.listen(port, () => {
  console.log(`App running on port ${port} ..`);
});
